import java.util.ArrayList;
import java.util.Random;

/**
 * University of San Diego
 * COMP 285: Spring 2015
 * Instructor: Gautam Wilkins
 *
 * Implement your Connect Four player class in this file.
 */
public class MyPlayer extends Player {

    Random rand;

    private class Move {
        int move;
        double value;

        Move(int move) {
            this.move = move;
            this.value = 0.0;
        }
    }

    public MyPlayer() {
        rand = new Random();
        return;
    }

    public void setPlayerNumber(int number) {
        this.playerNumber = number;
    }


    public int chooseMove(Board gameBoard) {

        long start = System.nanoTime();

        Move bestMove = search(gameBoard, 7, this.playerNumber);
        System.out.println(bestMove.value);

        long diff = System.nanoTime()-start;
        double elapsed = (double)diff/1e9;
        System.out.println("Elapsed Time: " + elapsed + " sec");
        return bestMove.move;
    }

    public Move search(Board gameBoard, int maxDepth, int playerNumber) {

        ArrayList<Move> moves = new ArrayList<Move>();

        // Try each possible move
        for (int j=0; j<Board.BOARD_SIZE; j++) {

            // iterate from center column
            // so it will place the tile on the center column
            // unless it finds a better move
            int i = (j+3)%Board.BOARD_SIZE;

            // iterate from right to left
            i = 6 - i;

            // Skip this move if the column isn't open
            if (!gameBoard.isColumnOpen(i)) {
                continue;
            }

            // Place a tile in column i
            Move thisMove = new Move(i);
            gameBoard.move(playerNumber, i);

            // Check to see if that ended the game
            int gameStatus = gameBoard.checkIfGameOver(i);
            if (gameStatus >= 0) {

                if (gameStatus == 0) {
                    // Tie game
                    thisMove.value = 0.0;
                } else if (gameStatus == playerNumber) {
                    // Win
                    // higher value for less moves needed
                    thisMove.value = maxDepth;

                } else {
                    // Loss
                    thisMove.value = -maxDepth;
                }

            } else if (maxDepth == 0) {
                // If we can't search any more levels down then apply a heuristic to the board
                thisMove.value = heuristic(gameBoard, playerNumber);

            } else {
                // Search down an additional level
                Move responseMove = search(gameBoard, maxDepth-1, (playerNumber == 1 ? 2 : 1));
                thisMove.value = -responseMove.value;
            }

            // Store the move
            moves.add(thisMove);

            // Remove the tile from column i
            gameBoard.undoMove(i);
        }

        // Pick the highest value move

        return this.getBestMove(moves);

    }

    public double heuristic(Board gameBoard, int playerNumber) {
        // This should return a number between -1.0 and 1.0.
        //
        // If the board favors playerNumber then the return value should be close to 1.0
        // If the board favors playerNumber's opponent then the return value should be close to 1.0
        // If the board favors neither player then the return value should be close to 0.0
        int board[][] = gameBoard.getBoard();
        int preCell = 0;
        int row, column;
        int count = 0;
        double value = 0.0;
        // check 3 in a row for player
        for(row = 0; row < 7; row++){
            for(column = 0; column < 7; column ++){
                if(board[row][column] == playerNumber){
                    if(preCell != playerNumber){
                        count = 1;
                    }else{
                        count++;
                    }
                    preCell = playerNumber;
                }else{
                    preCell = 0;
                }
                if(count == 3){
                    value = value + 1.0;
                    count = 0;
                }
            }
        }

        for(column = 0; column < 7; column++){
            for(row = 0; row < 7; row ++){
                if(board[row][column] == playerNumber){
                    if(preCell != playerNumber){
                        count = 1;
                    }else{
                        count++;
                    }
                    preCell = playerNumber;
                }else{
                    preCell = 0;
                }
                if(count == 3){
                    value = value + 1.0;
                    count = 0;
                }
            }
        }

        // check for opponent
        int opNumber;
        if(playerNumber == 1){
            opNumber = 2;
        }else{
            opNumber = 1;
        }

        for(row = 0; row < 7; row++){
            for(column = 0; column < 7; column ++){
                if(board[row][column] == opNumber){
                    if(preCell != opNumber){
                        count = 1;
                    }else{
                        count++;
                    }
                    preCell = opNumber;
                }else{
                    preCell = 0;
                }
                if(count == 3){
                    value = value - 1.0;
                    count = 0;
                }
            }
        }

        for(column = 0; column < 7; column++){
            for(row = 0; row < 7; row ++){
                if(board[row][column] == opNumber){
                    if(preCell != opNumber){
                        count = 1;
                    }else{
                        count++;
                    }
                    preCell = opNumber;
                }else{
                    preCell = 0;
                }
                if(count == 3){
                    value = value - 1.0;
                    count = 0;
                }
            }
        }


        // make sure value smaller than 1.0
        value = value/20;
        return value;
    }


    private Move getBestMove(ArrayList<Move> moves) {

        double max = -7.0;
        Move bestMove = moves.get(0);

        for (Move cm : moves) {
            if (cm.value > max) {
                max = cm.value;
                bestMove = cm;
            }
        }

        return bestMove;
    }



}
